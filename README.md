# README #

README file for reproducing results in:  
**Formation and stability of same-dip double subduction systems**  
submitted to Journal of Geophysical Research - Solid Earth

Authors: Adina E. Pusok, Dave R. Stegman  
Affiliation: Scripps Institution of Oceanography, UCSD  
Corresponding author: apusok@ucsd.edu (A.E. Pusok).

### Structure of Repository ###
      
* Figures
* Input_Files
* Model\_Setup

### Code and dependencies ###

[LaMEM](https://bitbucket.org/bkaus/lamem/branch/cvi_test) version and dependencies (Kaus et al., 2016).
LaMEM version used: cvi\_test branch latest commit

LaMEM source code/branches are not provided in this archive! Please download them from the links provided.
LaMEM and the cvi_branch can be downloaded from Bitbucket using ‘git clone’.

The following dependencies are needed to install LaMEM and reproduce results: petsc3.7.5, gcc6 and mpi compilers (available through Macports on MAC OSX). Installation details are located in: LaMEM (cvi_test)/doc/installation, and in the LaMEM repository wiki page.

### Model Setup ###

The model setups (i.e particle files) were created in Matlab. The Matlab scripts for this study can be found in Model\_Setup/. Executing them will produce the particle files for the different geometries needed. <br/>
Preview of initial geometry is possible in Paraview with the option:  
Paraview_output = 1;

Simulations were performed on 4-16 cpus (2-D) and 512cpu (3-D) on a desktop iMac (UCSD), Comet (SDSC), Stampede2 (UTACC, Texas).

### Input Files ###

(LaMEM Parameter Files)

Every simulation can be reproduced by running each parameter file with the respective particle input geometry.
The input geometry obtained before is specified in each file as:  
LoadInitialParticlesDirectory  = ../../Input/MatlabInputParticles

General command to run LaMEM (may change on high-performance clusters):  
mpiexec -n 16 ./*path to exec*/LaMEM -ParamFile Setup_IndiaAsia_2D -restart 1

### Visualization and Post-processing ###

Performed in Paraview and Matlab. Figures/ contains low-resolution output files for every simulation in this study.

Please contact the main author for questions and access regarding the post-processing work.

### Extended Archive (request from A.E.P.)###

Structure of extended archive (298 Gb):  
2.5G    a06b01.tgz  
2.7G    a06b02.tgz  
2.5G    a06b03.tgz  
2.6G    a06b04.tgz  
1.8G    a06b05.tgz  
2.5G    a06c01.tgz  
4.5G    a07b01LM01.tgz  
6.1G    a07b01LM02.tgz  
5.9G    a07b01LM03.tgz  
6.6G    a07b01LM04.tgz  
6.5G    a07b01LM05.tgz  
7.7G    a07b02LM02.tgz  
7.7G    a07b03LM02.tgz  
7.7G    a07b04LM02.tgz  
3.6G    a07b05LM02.tgz  
6.2G    a07c01LM02.tgz  
6.1G    a08b01LM02B01.tgz  
4.6G    a08b01LM02B02.tgz  
4.6G    a08b01LM02B03.tgz  
4.6G    a09b01LM02C01.tgz  
4.6G    a09b01LM02C02.tgz  
4.6G    a09b01LM02C03.tgz  
2.4G    a10b00S01.tgz  
2.5G    a10b00S02.tgz  
2.4G    a10c00S01.tgz  
2.5G    a10c00S02.tgz  
4.6G    a11b00S01LM.tgz  
4.6G    a11b00S02LM.tgz  
4.6G    a11c00S01LM.tgz  
4.6G    a11c00S02LM.tgz  
 16G    a12b00\_reduced.tgz  
 19G    a12b01\_reduced.tgz  
 18G    a12b04\_reduced.tgz  
 33G    a13b00\_reduced.tgz  
 22G    a13b01\_reduced.tgz  
 20G    a13b04\_reduced.tgz  
 
To unarchive a .tgz file, execute from the command line:  
tar xvzf a06b01.tgz