%%% Velocity influx profiles %%%

% Time
tstop = 10; %Ma
time  = 0:0.1:tstop;
time2 = 0:1:tstop;

% slope coeff
a = 0.7;
b = 7;

% Velocity
real_velin1 =  7*ones(size(time));
real_velin2 = 14*ones(size(time));
real_velin3 = a.*time+b; %lin
real_velin4 = [7 9 11 13 15 17 19 19.7 20 20 19.5];

% Plotting
font_size = 14;

% Theoretical
figure(1); clf
hold on
grid on
box on

set(gca,'FontSize',font_size);

h1 = plot(time,real_velin1,'k-','LineWidth',2);
h2 = plot(time,real_velin2,'b-','LineWidth',2);
h3 = plot(time,real_velin3,'r-','LineWidth',2);
h4 = plot(time2,real_velin4,'c-','LineWidth',2);

xlabel('Time (Myr)','FontSize',font_size);
ylabel('Maximum amplitude (km)','FontSize',font_size);
axis([0 tstop*4 0 25]);

h = legend([h1 h2 h3 h4],{'f=const', 'f=const', 'f=ax+b', 'f=real'},'Location','NorthEast');
set(gca,'FontSize',font_size);
grid on

set(gcf, 'Position', [0, 0, 751, 482]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 7.51 4.82])

% % save figure
Name2 = ['influx_velocity.png'];
print(figure(1),Name2,'-dpng');

% Numerical